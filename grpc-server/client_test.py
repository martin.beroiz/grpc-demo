import grpc
from echo_pb2 import EchoRequest
from echo_pb2_grpc import EchoServiceStub

secret_message = "LowLatency"
channel = grpc.insecure_channel("localhost:9090")
client = EchoServiceStub(channel)
request = EchoRequest(message=secret_message)
response = client.Echo(request)

if response.message == secret_message:
    print("Success!!")
else:
    print(f"Something went wrong. Message Received: {response.message}")
