### gRPC Server

Note: This step is only needed if you modify the protobuf `protobuf/echo.proto`.
In that case, you need to regenerate the `echo_pb2*` python files.

Install protobuf compilers for python:

```shell
python -m pip install grpcio-tools
```

Compile the protobufs:

```shell
python -m grpc_tools.protoc -I../protobufs --python_out=. --pyi_out=. --grpc_python_out=. ../protobufs/echo.proto
```

This will generate `echo_pb2_grpc.py`, `echo_pb2.py` and `echo_pb2.pyi` files.

### Build the docker image

```shell
docker build -t grpcserver:v1.0 .
```

### Test the docker with the client test

Run the container

```shell
docker run -it -p 9090:9090 --name grpcserve grpcserver:v1.0
```

From another shell run the `client_test` script.

```shell
python client_test.py
Success!!
```

Stop the container when done.
