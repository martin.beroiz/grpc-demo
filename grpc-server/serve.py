from concurrent import futures 
import grpc

from echo_pb2 import EchoResponse, EchoRequest
import echo_pb2_grpc


class EchoService(echo_pb2_grpc.EchoServiceServicer):
    def Echo(self, request, context):
        return EchoResponse(message=request.message)

def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    echo_pb2_grpc.add_EchoServiceServicer_to_server(
        EchoService(), server
    )
    server.add_insecure_port("[::]:9090")
    server.start()
    server.wait_for_termination()


if __name__ == "__main__":
    serve()
