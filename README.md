# gRPC on the web Demo

Note: The gRPC server in this demo is based on `Python` language
but it could be `node` or `Rust` or any other language.

## Run the demo with docker-compose

Launch all the services with docker-compose

```shell
docker-compose up
```

and test the app served at http://localhost:8010.

## Manual config/run

### Compile all the services

Before running the containers, we must create/compile the

1. Envoy proxy docker image (Follow README in `envoy-proxy` folder)
2. gRPC server docker image (Follow README in `grpc-server` folder)
3. gRPC/javascript web app. (Follow README in `web-app` folder)

### Run the services

We need to run all services on the same network (grpcnet).

1. After compiling the `grpc-server` docker image, launch the grpc server.

    ```shell
    $ docker run -itd --net grpcnet -p 9090:9090 --name grpcserve grpcserver:v1.0
    ```

2. After compiling the `envoy-proxy` docker image, launch the envoy-proxy server

    ```shell
    $ docker run -itd --net grpcnet -p 9901:9901 -p 8080:8080 --name envoy envoy:v1.0
    ```

    * Check that the admin interface works at http://localhost:9901

3. Open `web-app/public/index.html` in your browser and test the app!
