const { EchoRequest, EchoResponse } = require("./echo_pb.js");
const { EchoServiceClient } = require("./echo_grpc_web_pb.js");

var echoService = new EchoServiceClient("http://localhost:8080");

$(document).ready(function () {
  $("#send-button").on("click", function (e) {
    e.preventDefault();
    var word = $("#echo-phrase").val();
    var request = new EchoRequest();
    request.setMessage(word);

    // Send word over to echo service
    echoService.echo(request, {}, function (err, response) {
        if (err) {
            console.log(err);
        }
        console.log("Message Received should be", word);
        console.log(response.getMessage());
        $("#echo-response-box").html(response.getMessage());
      });
  });
});
